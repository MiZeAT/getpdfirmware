#get-PDFirmware.ps1



#check HPE ILO Modules
#source: https://www.powershellgallery.com/packages/HPEiLOCmdlets
if (!(Get-Module -ListAvailable HPEiLOCmdlets)) {Install-Module -Name HPEiLOCmdlets -Confirm:$false}


#import ILO Data
$myilos=Import-Csv -Path ./iLOInput.csv

#go
foreach ($ilo in $myilos) {
    Write-Host "Connecting to ilo" $ilo.IP -ForegroundColor Green
    $connection = Connect-HPEiLO -IP $ilo.IP -Username $ilo.Username -Password $ilo.Password -DisableCertificateAuthentication
    Write-Host "Checking SmartArrayStorageController ..." -ForegroundColor Green
    $MySmartArray=Get-HPEiLOSmartArrayStorageController -Connection $connection -Verbose
    

    $myFilename = "./out_" + $MySmartArray.Hostname
    

        #export xlsx or csv
    #if ImportExcel ist installed, export as XLSX also 
    # Find-Module ImportExcel |Install-Module
    # source https://www.powershellgallery.com/packages/ImportExcel/
    if ((Get-Module -ListAvailable ImportExcel)) {
        Write-Host "Export xlsx to" $myFilename -ForegroundColor Green
        $MySmartArray.Controllers[0].PhysicalDrives |Select-Object Id,MediaType,CapacityGB,Location,Model,SerialNumber,FirmwareVersion,InterfaceType,InterfaceSpeedMbps,Name,Description |Export-Excel -Path ($myFilename+".xlsx")
        #Append to out_ALL.xlsx
        $MySmartArray.Controllers[0].PhysicalDrives |Select-Object Id,MediaType,CapacityGB,Location,Model,SerialNumber,FirmwareVersion,InterfaceType,InterfaceSpeedMbps,Name,Description |Export-Excel -Path "./out_ALL.xlsx" -Append

    }
    else {
        Write-Host "Export csv to" $myFilename -ForegroundColor Green
        $MySmartArray.Controllers[0].PhysicalDrives |Select-Object Id,MediaType,CapacityGB,Location,Model,SerialNumber,FirmwareVersion,InterfaceType,InterfaceSpeedMbps,Name,Description |Export-Csv -Path ($myFilename+".csv")
    }

    Write-Host "Disconnecting" $ilo.Ip -ForegroundColor Green
    Disconnect-HPEiLO -Connection $Connection
}




