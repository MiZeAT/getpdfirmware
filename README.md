# getPDfirmware

Get physical Disk/SSD Firmware

# Prerequisites

    * HPEiLOCmdletsfrom https://www.powershellgallery.com/packages/HPEiLOCmdlets/
      Install-Module -Name HPEiLOCmdlets
    * (optional) importexcel module powershell from  https://www.powershellgallery.com/packages/ImportExcel/
    


# Filf iLOInput.csv 
- fill iLOInput.csv with ILO$ oder ILO% name/IPs,user,password

# How to use
- Execute
`.\getPDFirmware.ps1

